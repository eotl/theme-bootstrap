/* meaning.js - explains some of what EOTL means */
var meanings = {
    "one": {
        "headline": "End Of The Line",
        "url": "https://eotl.supply"
    },
    "two": {
        "headline": "Excited Other Techniques Living",
        "url": "https://en.wikipedia.org/wiki/Freight_bicycle"
    },
    "three": {
        "headline": "Elegant Options Towards Life",
        "url": "https://original-unverpackt.de"
    },
    "four": {
        "headline": "Ecological Obstacles Terrorizing Life",
        "url": "https://public.wmo.int/en/media/press-release/wmo-climate-statement-past-4-years-warmest-record"
    },
    "five": {
        "headline": "Ecocide On The Land",
        "url": "https://www.theguardian.com/commentisfree/2019/mar/15/climate-strikers-urgency-un-summit-world-leaders"
    },
    "six": {
        "headline": "Extinquishing Other Terains Longevity",
        "url": "https://www.nature.com/articles/s41467-018-05252-y"
    },
    "seven": {
        "headline": "Ending Of Tranquil Life",
        "url": "https://www.nature.com/articles/d41586-019-02782-3"
    },
    "eight": {
        "headline": "Eat Off The Land",
        "url": "https://markthalleneun.de"
    },
    "nine": {
        "headline": "Extinction Of The Living",
        "url": "https://climatepeopleorg.com/2016/05/04/the-climate-crisis-in-quotations/"
    },
    "ten": {
        "headline": "Empowering Open Technological Liberation",
        "url": "https://codeberg.org/eotl"
    },
    "eleven": {
        "headline": "Electricity Obtained To Light",
        "url": "https://eotl.solar"
    },
    "tweleve": {
        "headline": "Ecstatic Objects That Live",
        "url": "https://morningchores.com/mushroom-farming/"
    },
    "thirteen": {
        "headline": "Egalitarian Open Together Life",
        "url": "https://www.aardehuis.nl"
    },
    "fourteen": {
        "headline": "Explore Outwardly Towards Links",
        "url": "https://eotl.links"
    },
    "fifteen": {
        "headline": "Elevate Orientate Towards Liberation",
        "url": "https://www.ted.com/talks/ron_finley_a_guerrilla_gardener_in_south_central_la"
    },
    "sixteen": {
        "headline": "Emerge Organize Towards Luminesscence",
        "url": "https://solargis.info/imaps/"
    },
    "seventeen": {
        "headline": "Everywhere Organisms Trip Loudly",
        "url": "https://www.france24.com/en/live-news/20220603-global-plastic-waste-on-track-to-triple-by-2060"
    },
    "eighteen": {
        "headline": "Exceptional Organisms Test Limits",
        "url": "https://www.npr.org/2017/02/18/514523987/when-their-food-ran-out-these-reindeer-kept-digging"
    }
}

var meaningArray = Object.keys(meanings)

function meaningWhat() {
    var position = meaningArray[meaningArray.length-1]
    var tickerContainer = $(".meaning-cycle")
    var meaningText = $(".meaning-text")

    tickerContainer.fadeIn(1000, function() {
        $(this).delay(4500).fadeOut('fast', meaningWhat)
    })
    meaningText.html('&nbsp;')

    var piece = meanings[position].headline.split(' ')

    setTimeout(function() {
        meaningText.append(piece[0])
    }, 1000)

    setTimeout(function() {
        meaningText.append(' ' + piece[1])
    }, 1500)

    setTimeout(function() {
        meaningText.append(' ' + piece[2])
    }, 2000)
    setTimeout(function() {
        meaningText.append(' ' + piece[3])
    }, 2500)

    meaningText.wrap("<a class='meaning-link' href='" + meanings[position].url + "' target='_blank'></a>")
    meaningArray.unshift(position)
    meaningArray.pop()
}
