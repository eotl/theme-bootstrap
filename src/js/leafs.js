/* showLeafs() - based on code taken from source https://micro.blog/about */
const colors = [ "#008000", "#00b000", "#76a147", "#9af092", "#cacd4c", "#6ec100", "#258341", "#5a6429", "#be9132", "#b6975f" ];

function hexToRGBA(hex, opacity) {
    hex = hex.replace(/^#/, ""); // remove "#" if present
    var r = parseInt(hex.substring(0, 2), 16);
    var g = parseInt(hex.substring(2, 4), 16);
    var b = parseInt(hex.substring(4, 6), 16);
    return "rgba(" + r + "," + g + "," + b + "," + opacity + ")";
}
    
function showLeafs() {
    // hide the button and show features
    document.getElementById("feature_showmore").style.display = "none";
    document.getElementById("feature_bubbles").style.display = "block";
    
    // get all spans with ids 1 to 87
    var spans = [];
    for (var i = 1; i <= 87; i++) {
        spans.push(document.getElementById(i.toString()));
    }
f
    // make first item visible right away
    var first_item = spans[0];
    first_item.style.opacity = 1;

    // loop over each span
    for (var i = 0; i < spans.length; i++) {
        (function(i) {
            var span = spans[i];
            var random_color = colors[i % colors.length]; // cycle through colors if there are more spans than colors

            // set color right away, half opacity
            span.style.backgroundColor = hexToRGBA(random_color, 0.5);

            // random delay between 0 and 3 seconds
            var random_delay = Math.random() * 3000;

            // set a timeout to change the background color after the random delay
            setTimeout(function() {
                span.style.opacity = 1;
            }, random_delay);
        })(i);
    }
}
