/*!
  * eotl.js - https://eotl.network
  * Copyright 2020-2021 EOTL Authors (https://codeberg.org/eotl/theme-bootstrap)
  * Licensed under ISC (https://codeberg.org/eotl/theme-bootstrap/raw/branch/main/LICENSE)
  */

function longNowify(elements) {
    let itemDate = '';
    for (element in elements) {
        itemDate = elements[element].innerHTML
        if (itemDate) {
            var year = itemDate.match(/(?:01|20)\d{2}/g);
            var longYear = '0' + year[0]
            var fixed = itemDate.replace(year[0], longYear)
            elements[element].innerHTML = fixed
            elements[element].setAttribute("title", "Extra '0' to question: how long is now?")
        }
    }
}
