/* switcher.js - toggles between light and dark modes */

function setTheme(themeName) {
    localStorage.setItem('theme', themeName);
    document.documentElement.className = themeName;
}

// Toggle between modes
function toggleTheme() {
    var btnSwitch = document.getElementById('switch')
    if (localStorage.getItem('theme') === 'light-mode') {
        setTheme('dark-mode')
        var newHTML = `
            <img class="switch-img" src="/images/theme-dark.svg" title="switch theme" width="auto" height="14">
            <span>Light</span>`
    } else {
        setTheme('light-mode')
        var newHTML = `
            <img class="switch-img" src="/images/theme-light.svg" title="switch theme" width="auto" height="14">
            <span>Dark</span>`
    }
    btnSwitch.innerHTML = newHTML
}

// Set theme on load dark-mode by default
document.addEventListener('DOMContentLoaded', function() {
    (function() {
        var btnSwitch = document.getElementById('switch')
        if (localStorage.getItem('theme') === 'light-mode') {
            setTheme('light-mode')
            var newHTML = `
                <img class="switch-img" src="/images/theme-dark.svg" title="switch theme" width="auto" height="14">
                <span>Dark</span>`
        } else {
            setTheme('dark-mode')
            var newHTML = `
                <img class="switch-img" src="/images/theme-light.svg" title="switch theme" width="auto" height="14">
                <span>Light</span>`
        }
        if (btnSwitch != null && btnSwitch.innerHTML != "") {
            btnSwitch.innerHTML = newHTML;
        }
    })()
})
