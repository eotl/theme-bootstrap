#! /bin/bash
#
# Simple build script for SASS assets and fonts with file watcher for SASS
#
# Dependencies:
#
#   apt install sassc inotify-tools

ROOT_PATH=$(pwd)
SRC_PATH=$(pwd)/src
WATCH_PATH=$SRC_PATH
FETCH_ICONS=false

# Default or custom
if [[ $1 != "" && -d $1 ]]; then
    BUILD_PATH=$1
else
    BUILD_PATH=$(pwd)/dist
fi

function cleanBuild {
    echo -e "- Cleaning build (${BUILD_PATH})"
    rm -rf $BUILD_PATH/*
    sleep 2
}

function waitForChange {
    inotifywait -q --format %f -r -e create -e moved_to -e modify $WATCH_PATH
    echo "--------------------------------------------------------------------"
}

function renderSass {
    sleep 1
    if ! command -v sassc &> /dev/null
    then
        # For OSX
        sass $SRC_PATH/scss/eotl.scss:$PATH_CSS/eotl.css --style=compressed
        echo -e "- Done building SASS\n"
    else
        # For Linux
        sassc $SRC_PATH/scss/eotl.scss -t compact $PATH_CSS/eotl.css
        echo -e "- Done building SASS\n"
    fi

    # Copy styleguide
    cat $SRC_PATH/outreach.txt > dist/index.html
    cat $SRC_PATH/index.html >> dist/index.html
    cp $SRC_PATH/favicon.ico dist/
}

function copyAssets {
    # Build to /your/path
    if [[ $1 != "" && -d $1 ]]; then
        BUILD_PATH="${1}"
        echo -e "- Build to: ${BUILD_PATH}"
    else
        echo -e "- Build default: ${BUILD_PATH}/"
        cp $SRC_PATH/index.html dist/
        cp $SRC_PATH/favicon.ico dist/
    fi

    # Paths
    PATH_CSS="${BUILD_PATH}/css"
    PATH_FONTS="${BUILD_PATH}/fonts"
    PATH_ICONS="${BUILD_PATH}/icons"
    PATH_IMAGES="${BUILD_PATH}/images"
    PATH_JS="${BUILD_PATH}/js"

    # CSS
    if [[ ! -d $PATH_CSS ]]; then
        mkdir -p $PATH_CSS
    fi
    cp $SRC_PATH/css/styleguide.css dist/css/

    # Fonts
    if [[ ! -d $PATH_FONTS ]]; then
        mkdir -p $PATH_FONTS
    fi
    cp $SRC_PATH/fonts/* $PATH_FONTS/
    echo -e "- Done copying: fonts"

    # Icons
    SOURCE_ICONS="${ROOT_PATH}/icons"
    if [[ $FETCH_ICONS == true ]]; then
        if [[ ! -d $SOURCE_ICONS ]]; then
            echo -e "- Fetching icons repo\n"
            git clone --depth=1 https://codeberg.org/eotl/icons.git $SOURCE_ICONS
            echo -e ""
        else
            cd $SOURCE_ICONS
            echo -e "- Updating icons repo\n"
            git pull origin main
            echo -e ""
            cd $ROOT_PATH
        fi
    fi

    if [[ -d $SOURCE_ICONS ]]; then
        mkdir -p $BUILD_PATH/icons
        cp $SOURCE_ICONS/dist/*.svg $BUILD_PATH/icons/
        echo -e "- Done copying: icons"
    else
        echo -e "- Skipped icons non-existent"
    fi

    # Images
    if [[ ! -d $PATH_IMAGES ]]; then
        mkdir -p $PATH_IMAGES
    fi
    cp $SRC_PATH/images/* $PATH_IMAGES/
    echo -e "- Done copying: images"

    # JS
    if [[ ! -d $PATH_JS ]]; then
        mkdir -p $PATH_JS
    fi
    cp $SRC_PATH/js/bootstrap*.js $PATH_JS/
    cp $SRC_PATH/js/jquery*.js $PATH_JS/
    cp $SRC_PATH/js/popper*.js $PATH_JS/

    JS_FILES_EOTL="${SRC_PATH}/js/eotl.js ${SRC_PATH}/js/meaning.js ${SRC_PATH}/js/switcher.js ${SRC_PATH}/js/leafs.js"
    cat $JS_FILES_EOTL > $PATH_JS/eotl.js 
    echo -e "- Done copying: js"
}

while (( ${#} > 0 )); do
    case "${1}" in
      ('--clean') CLEAN="true";;
      ('--icons') FETCH_ICONS="true";;
      ('--watch') WATCH="true";;
      ('--publish') PUBLISH="true";;
      ('--') OPANDS+=("${@:2}"); break;;
      ('-'?*) ;;
      (*) OPANDS+=("${1}")
    esac
    shift
done

if [[ $CLEAN == true ]]; then
    cleanBuild
fi

mkdir -p $BUILD_PATH
copyAssets $BUILD_PATH
renderSass

if [[ $WATCH == true ]]; then
    echo -e "Watching for changes: ${WATCH_PATH}"
    while waitForChange; do
        renderSass
    done
fi

if [[ $PUBLISH == true ]]; then
    echo -e "Checking and updating icons (if needed)."
    cd icons/
    git pull
    cd ../

    echo -e "Now publishing to NPM"
    sleep 1
    npm publish
fi
