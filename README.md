# EOTL Theme

A minimalist theme for EOTL sites and apps that defaults to dark mode.
Dependencies which are bundled with the theme are:

- [Bootstrap](https://getbootstrap.com) v4
- [jQuery](https://jquery.com) v3.6.0
- Roboto and Roboto Mono font families
- EOTL [iconset](https://codeberg.org/eotl/icons)


## Installing

Using those fancy javascript tools, you can install the theme with:

```
$ npm install @eotl/theme-bootstrap
$ yarn add @eotl/theme-bootstrap
```

Or for a more simple approach, you can just `wget` the tar file.

```
$ wget https://registry.npmjs.org/@eotl/theme-bootstrap/-/theme-bootstrap-0.1.5.tgz
```

Or more simple still [download theme](https://registry.npmjs.org/@eotl/theme-bootstrap/-/theme-bootstrap-0.1.5.tgz)


## Building

Default is to build is with the [sassc](https://github.com/sass/sassc) utility.
We created an build script to assist with various needs. First make sure you 
have a `sass` compiler installed

```
# Linux
$ sudo apt install sassc
# OSX
npm install -g sass
```

Clone repo and build your assets!

```
$ git clone git@codeberg.org:eotl/theme-bootstrap.git
$ cd theme-bootstrap/
$ ./build.sh --icons
```

The build structure looks like this:

```
dist/
 ├─ css/
 │  └─ eotl.css
 ├─ fonts/
 │  ├─ Roboto-Black.ttf
 │  └─ RobotoMono-ThinItalic.ttf
 ├─ icons/
 ├─ images/
 ├─ js/
 ├─ favicon.ico
 └─ index.html
```

- The `index.html` is a styleguide which shows all styled components
- The `/css/eotl.css` file loads the font files via the `/fonts` path.


## Developing

To contribute to the theme, you can get the source to rebuild on changes, 
you just need to install the `inotify-tools` package and use the `--watch`

```
$ sudo apt install inotify-tools
$ ./build.sh --watch
```

Or specify a build path

```
$ ./build.sh --watch /path/to/site/
```


## Packaging

To publish to NPM (assuming you have credentials) do the following:

```
$ npm pack
$ npm publish
```

